# mrGPSM
The intrusive auditory model multi-resolution generalized power spectrum model (mrGPSM; Biberger and Ewert, 2017)
can be applied to predict speech intelligibilty or psychoacoustic experiments. The model requires
target+noise and noise as input signals. Here it is important to know that the target+noise signal and the noise signal 
use the same noise signal (frozen noise). The target signal could be a target modulation (psychoacoustic experiments) or 
speech (speech intelligibilty experimemts).

The mrGPSM output provides three submeasures:
* '**combined_SNR**':         based on the combination of the envelope power SNR (SNR_ac)
                              and the power SNR (SNR_dc)
* '**SNR_ac**':               envelope power SNR
* '**SNR_dc**':               power (intensity) SNR

'example_mrGPSM_simple_Psy.m' and 'example_mrGPSM_simple.m' give minimal examples how to use the mrGPSM for psychoacoustic and speech intelligbility experiments in Matlab.
See 'General usage for SI predictions' below for a more detailed description of how to run the model in a more complex simulation framework (an example is given in 'example_mrGPSM_complx.m'), calibrate the model, 
and obtain predicted SRT50.


A more detailed description of the mrGPSM is given in:

T. Biberger and S.D. Ewert, "The role of short-time intensity and envelope
power for speech intelligibility and psychoacoustic masking", Journal of the 
Acoustical Society of America, vol. 142, no. 2, pp.1098-1111. 2017.
http://dx.doi.org/10.1121/1.4999059

**Abstract:**  
The generalized power spectrum model [GPSM; Biberger and Ewert (2016). J. Acoust. Soc. Am.
140, 1023-1038], combining the "classical" concept of the power-spectrum model (PSM) and the
envelope power spectrum-model (EPSM), was demonstrated to account for several psychoacoustic
and speech intelligibility (SI) experiments. The PSM path of the model uses long-time power
signal-to-noise ratios (SNRs), while the EPSM path uses short-time envelope power SNRs. A systematic
comparison of existing SI models for several spectro-temporal manipulations of speech
maskers and gender combinations of target and masker speakers [Schubotz et al. (2016). J. Acoust.
Soc. Am. 140, 524-540] showed the importance of short-time power features. Conversely,
Jørgensen et al. [(2013). J. Acoust. Soc. Am. 134, 436-446] demonstrated a higher predictive
power of short-time envelope power SNRs than power SNRs using reverberation and spectral
subtraction. Here the GPSM was extended to utilize short-time power SNRs and was shown to
account for all psychoacoustic and SI data of the three mentioned studies. The best processing strategy
was to exclusively use either power or envelope-power SNRs, depending on the experimental
task. By analyzing both domains, the suggested model might provide a useful tool for clarifying the
contribution of amplitude modulation masking and energetic masking.



## General usage for SI predictions:
1. Run the file 'example_mrGPSM_complx.m' (located in folder 'examples') to obtain predictions (combined_SNR, SNR_ac, SNR_dc)
   for target speech sentences (OLSA) presented in speech-shaped noise (SSN) and sinusoidal
   amplitude modulated noise (SAM) maskers at SNRs ranging from -27 dB to -3 dB. Combined_SNR, SNR_ac, and SNR_dc are calculated for
   for each of the three OLSA senteces and each SNR (between target sentence and masker) and stored in 
   'mrGPSM_target_male_masker_male_3sentences_SSN_1.mat' and 'mrGPSM_target_male_masker_male_3sentences_SAM_SSN_1.mat'.
   This framework can easily be extended for any other masker type.  
   Check in file 'example_mrGPSM_complx.m' whether the front end settings for psychoacoustic ( def.predictions='PSY')
   or speech intelligibility (def.predictions='SI') have been selected. Use def.predictions='SI'. 
2. In the next step, predicted and measured SRTs from stationary SSN masker are used to calibrate the model. This can be done
   by using either 'Plot_SSN.m' or 'main_plot_script_mrGPSM.m' (more straighforward). 
2.1 Using 'Plot_SSN.m': First load the file with predictions for the SSN masker ('mrGPSM_target_male_masker_male_3sentences_SSN_1.mat').
    Then you have to adjust the psychometric function of the model predictions to the measured psychometric function, by using
    parameters k, q, m and sigma_s. There is not always the completely measured psychometric function available, but the measured SRT50.  
    In that case, the psychometric function from predictions has to be adjusted to the SRT50 by varying the parameter k and maintaining the default value for q of 0.5.
    Once the calibration parameters have been identified, they can be applied to 'main_plot_script_mrGPSM.m'. 
2.2 Using 'main_plot_script_mrGPSM.m': First load the file with predictions for the SSN masker ('mrGPSM_target_male_masker_male_3sentences_SSN_1.mat').
    Then follow the procedure as previously described in 2.1. SRT50s for SSN and SAM maskers are computed and displayed at the command window. This file 
    can be easily extended to automatically calculate SRT50s for futher masker conditions.




## Acknowledgements:

Author of the Matlab implementation of mrGPSM:  
thomas.biberger@uni-oldenburg.de


Following external Matlab functions were used within the mrGPSM.
* Gammatone filterbank from V. Hohmann, for details see (V. Hohmann, "Frequency analysis and 
  synthesis using a Gammatone filterbank", Acta Acustica united with Acustica, vol. 88, no.3, 
  pp. 433-442. 2002)
* 'MFB2.m' from Stephan D. Ewert and T. Dau
* 'IdealObserver_v1.m' and 'Plot_SSN.m' from Søren Jørgensen

