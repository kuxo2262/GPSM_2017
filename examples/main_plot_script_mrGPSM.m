function []=main_plot_script_mrGPSM();
% main_plot_script_mrGPSM.m

clc,close all

% both values are derived from the calibration condition (speech + ssn) by
% adjusting k and q so that predictions agree with data
params.k=0.72; % horizontal shift
params.q=0.5; % slope

% number of possible words (50 for the OLSA as closed sentence test)
params.m=50;
% sigma s (related to the redundancy of the speech material)
params.sigma_s=0.6;
% number of sentences per speech reception threshold
params.Nsentences=3;

% select condition
params.condition.SSN='on'; % 'on','off'
params.condition.SSN_SAM='on'; % 'on','off'

% filename of the SSN (reference) condition
params.condition.filename_SSN='mrGPSM_target_male_masker_male_3sentences_SSN_1';

params=filename_sep(params);

%% SSN
if strcmp(params.condition.SSN,'on')==1
    [params]=SSN_plot_fun_OLSA(params);
else
end
%% SSN-SAM
if strcmp(params.condition.SSN_SAM,'on')==1
    [params]=SSN_SAM_plot_fun_OLSA(params);
else
end
end


function [params]=filename_sep(params);
[idx]=find(params.condition.filename_SSN=='_');
params.condition.filename_plain=params.condition.filename_SSN(1:idx(end-1)-1);
params.condition.filename_no=params.condition.filename_SSN(idx(end)+1:length(params.condition.filename_SSN));
end