clc
clear all
close all;


%% define input
currentFile = mfilename( 'fullpath' );
[pathstr,~,~] = fileparts( currentFile );
addpath(genpath(fullfile( pathstr(1:end-9))));
define.cur_dir=fullfile(pathstr(1:end-9));
cd(define.cur_dir);

% gender of the target speaker: 'female' (stimuli for 'male' not available)
define.target.genders={'male'};
% gender of the masker: 'female' (stimuli for 'male' not available)
define.masker.genders={'male'};

% define masker types
%   'SSN':           - speech shaped noise
%   'SAM_SSN':       - sinusoidal amplitude modulated SSN
define.masker_types_all={'SSN','SAM_SSN'};

no_of_loops=3;  % number of loops in order to get some information about the variability of the stimuli

def.predictions='SI'; % usage of SI model version

for aa=1:no_of_loops;
    for bb=1:length(define.target.genders)             % select target gender
        for cc=1:length(define.masker.genders)         % select masker gender
            for ii=1:length(define.masker_types_all); % select masker type, e.g. SSN
                    
                cd(define.cur_dir);
                define.target.gender=define.target.genders{bb};
                define.masker.gender=define.masker.genders{cc};
                
                % evaluate whether the combination of target and masker
                % gender was measured in Schubotz et al. (2016)
                if  strcmp(define.target.gender,'female') && strcmp(define.masker.gender,'male') == 1
                    gender_comb=0;
                else
                    gender_comb=1;
                end
                
                % model predictions are carried out only for measured
                % conditions, otherwise conditions are skipped
                if gender_comb==1;
                    
                    %                 define.masker.gender=define.target.genders{bb}; % tb 22.08 nur f?r die WE berechnung
                    define.masker_type=define.masker_types_all{ii};
                    
                    disp(['*********************************************************']);
                    disp(['Start of ',define.masker_type]);
                    disp(['*********************************************************']);
                    
                    % create path for masker/target signals
                    [define]=define_target_masker_example(define);
                    
                    cd(define.target_path);
                    define.target.names=dir('*.wav');
                    
                    clear res x
                    NSpeechsamples = 3; % number of speech sentences, for this example only 2 sentences are provided
                    fs=22050;
                    for q = 1:NSpeechsamples
                        
                        % read in masker signal
                        if verLessThan('matlab','8.0')
                            [noise fs_mask]=wavread([define.masker_path,filesep,define.masker.soundfile]);
                        else
                            [noise fs_mask]=audioread([define.masker_path,filesep,define.masker.soundfile]);
                        end
                        
                        % read in target signal
                        if verLessThan('matlab','8.0')
                            [x fs_target]=wavread([define.target_path,filesep,define.target.names(q).name]);
                        else
                            [x fs_target]=audioread([define.target_path,filesep,define.target.names(q).name]);
                        end
                        
                        x=x(:,1);
                        
                        if fs_target ~= fs_mask,
                            error('signals have different sampling frequencies')
                        else
                            fs_sig=fs_target;
                        end
                          
                        % downsampling of target/masker signal to the required sampling freq. of the model  (here factor 2)
                        ds_fact=fs_sig/fs;
                        [noise]=resample(noise,1,ds_fact); % downsampling from 44100 Hz to 22050 Hz %
                        [x]=resample(x,1,ds_fact); % downsampling from 44100 Hz to 22050 Hz
                        
                        %                         Compute the length of the target signal
                        Ts = 1/fs;
                        T = length(x)/fs;
                        t = 0:Ts:T-Ts;
                        N = length(t);
                        
                        % select a noise snippet with the same length as the speech sentence
                        Nsegments = floor(length(noise)/N);
                        % pick a random segment from the noise file
                        startIdx = randi(Nsegments-2 ,1)*N;
                        noise = noise(startIdx:startIdx+N -1);
                        
                        % define the noise presentation level
                        SPL = 65; % noise presentation level
                        noise = (noise/rms(noise))*10^(((SPL-100))/20);
                        
                        % define the SNRs
                        SNRs = [-27 -24 -21 -18 -15 -12 -9 -6 -3];
                        
                        for k = 1:length(SNRs)
                            
                            % define the sentence presentation level
                            sentenceFileLevel = -24.00;
                            x_cal=x.*10^(((SPL-sentenceFileLevel+SNRs(k)-100))/20); % OLSA calib
                            
                            test = noise + x_cal; % mixture of speech + noise
                            %                             figure,plot(noise,'k')
                            %                             hold on
                            %                             plot(x_cal,'r')
                            %% here starts thomas insertion mrGPSM (monaural)
                            [tmp] = mrGPSM(test,noise,fs,def);
                            
                            %% ...here ends thomas' section                         
                            %         All data of the internal representation is stored in a struct:
                            res{k,q} = tmp; %{struct(['SNR_' num2str(k) '_cond_' num2str(n)] , tmp) };
                            
                        end
                        
                        disp(['sentence nr: ' num2str(q) ' ' datestr(now, 'dd-mm-yyyy HH:MM:SS')]);
                        
                    end
                    
                    saveName = ['mrGPSM_','target_',define.target.gender,'_masker_',define.masker.gender,'_',num2str(q), 'sentences_',define.masker_type,'_',num2str(aa)];
                    
                    result.res = res;
                    result.SNRs =SNRs;
                    save([define.cur_dir,filesep,saveName], 'result')
                     
                    disp(['*********************************************************']);
                    disp(['End of ',define.masker_type]);
                    disp(['*********************************************************']);
                    
                else
                    return
                end
            end
        end
    end
end