function [out] = mrGPSM(test,noise,fs,def)
% mrGPSM.m Main function for calculation speech intelligibility or
%          psychoacoustics thresholds based on test signal containing the
%          target + noise signals and the noise signal. For detailed
%          information see Biberger and Ewert (2017)
%  
% INPUT:    
%           test   ... target (speech or target modulation) + noise signal
%           noise  ... noise signal
%           fs     ... sampling frequency
% 
% OUTPUT:   
%           out    ... struct that contains the combined SNR, the envelope
%                      power based SNR and the power based SNR
% 
% 
% Usage: out = mrGPSM(test,noise,fs)
% authour: thomas.biberger@uni-oldenburg.de
% update: 2019-01-18; 2021-05-20


%% reorganize same variables
def.samplerate=fs; % sampling freq. in Hz
def.intervallen=length(test);% signal length in samples

%% define parameter/precalculate coefficients
[def simdef simwork]=mrGPSM_param_def(def);

%% Front end (feature) processing
[test_out work_test dc2mod_test] = mrGPSM_preproc(test,def,simdef,simwork);   % input for the speech signal + noise
[noise_out work_noise dc2mod_noise] = mrGPSM_preproc(noise,def,simdef,simwork);   % input for the noise signal


%% SNR computation and back end processing
[out]=  mrGPSM_feature_out(test_out,noise_out,work_test,work_noise,simwork, dc2mod_test,def);


end

