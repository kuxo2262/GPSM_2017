function [define]=define_target_masker_example(define);


%% female masker
if strcmp(define.masker.gender,'female')==1;
    
    
    % here's the place to insert the masker filenames based on female
    % speakers
    
    error('Please define a valid masker type condition');
    
    
    %% male masker
elseif strcmp(define.masker.gender,'male')==1;
    
    
    define.masker_path=[define.cur_dir,filesep,'wavs',filesep,'male_maskers'];
    
    if strcmp(define.masker_type,'SSN')==1;
        
        define.masker.soundfile='SSN_longtermspec_male_ISTS.wav';
        
    elseif strcmp(define.masker_type,'SAM_SSN')==1;
        
        define.masker.soundfile='SAM_SSN_longtermspec_male_ISTS.wav';
        
    else
        error('Please define a valid masker type condition');
    end
    
else
    error('Please define a valid masker condition');  
end

%% female target
if strcmp(define.target.gender,'female')==1;
    
    % here's the place to add the folder containing OLSA target sentences based on a male
    % speaker
    error('Only OLSA sentences based on a male speaker available.');
    
    %% male target
elseif strcmp(define.target.gender,'male')==1;
    
    define.target_path=[define.cur_dir,filesep,'wavs',filesep,'male_target'];
    
else
    error('Please define a valid target condition');
end