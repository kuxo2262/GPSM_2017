function [params]=SSN_SAM_plot_fun_OLSA(params)
% SSN_SAM_plot_fun_OLSA.m This function is based on the orignal function
%                         "Plot_SSN.m" by S�ren J�rgensen. It was modified 
%                         to give an automated estimation of the SRT50.
% 
% INPUT:
%       params ... struct that contains some pre-defined back end parameters
% 
% OUTPUT:
%       params ... struct that contains some pre-defined back end parameters
% 
% Usage: [params]=SSN_SAM_plot_fun_OLSA(params) 
% author: thomas.biberger@uni-oldenburg.de
% date: 2015-04-15
% update: 2019-02-19

NSpeechsamples = params.Nsentences;

filename=[params.condition.filename_plain,'_SAM_SSN_',params.condition.filename_no];

load (filename);
SNRs=result.SNRs;

IOparameters = [params.k params.q params.m params.sigma_s]; % parameters from J?rgensen et al., (2013).
% first parameter represents the constant k to calculate d' from the SNR
% second parameter reprents the constant q to calculate d' from the SNR
% third parameter represents the response-size set
% fourth parameter represents: sigma s (related to the redundancy of the speech material)

for q = 1:NSpeechsamples
    
    for k = 1:length(SNRs)
        SNR(k,q) = result.res{k,q}.SNR_comb;
    end
    Pc_est(:,q)  = IdealObserver_v1(SNR(:,q),IOparameters);
       
end

% Average across speech samples
Pc_est_mean_SAM = mean(Pc_est,2);
SNR_mean_SAM = mean(SNR,2);

% Estimation of the SRT50
SRT_pc=50;
[SRT]=SRT_est(SNRs,Pc_est_mean_SAM,SRT_pc);

disp(['SSN-SAM: SRT ',num2str(SRT_pc),' corresponds to : ',num2str(SRT), ' dB']);

%******************************************************
%******************************************************
%******************************************************



    fnts = 14;
    fig=figure;
    sh = 350;
    set(fig,'Position',[1.5*sh, 1.4*sh, 2.5*sh, 1*sh]);
    subplot(1,2,1)
    plot(SNRs,Pc_est_mean_SAM,'--d','color','k', 'markersize', 12),hold on  
    xlabel('SNRs (dB)','FontSize',fnts);
    ylabel('Percent correct %','FontSize',fnts);
    ylim([0 100])
    xlim([SNRs(1) 4])
    
    set(gca,'FontSize',fnts,'xtick',SNRs);
    lgnLables = {'mrGPSM'};
    
    le =legend(lgnLables);
    set(le,'box','off','location','southeast','fontsize',12)
    
    subplot(1,2,2)
    plot(SNRs,10*log10(SNR_mean_SAM),' -d','markersize',8,'color','k'), hold on
    xlabel('SNRs (dB)','FontSize',fnts);
    ylabel('combined SNR (dB)','FontSize',fnts);
    ylim([0 20])
    xlim([SNRs(1) 4])
   
    set(gca,'FontSize',fnts,'xtick',SNRs);
    
        
end 
   