function [out]=isothr_input(in,fs,simwork)

len_in=length(in);
in_freq=fft(in)/len_in;  % computation in freq. domain
in_freq_mag=abs(in_freq);           % only magnitude spectra
in_freq_phase=angle(in_freq);       % only phase spectra

if mod(len_in,2)==0 % even signal length
    vIsoThrDB=[simwork.vIsoThrDB simwork.vIsoThrDB(end-1:-1:2)];
elseif mod(len_in,2)==1 % odd signal length
    vIsoThrDB=[simwork.vIsoThrDB simwork.vIsoThrDB(end:-1:2)];
end


%% iso hearing threhold subtraction (subtracts hearing threshold curve (dB) from the input signal)

in_freq_dB=20*log10(in_freq_mag/(10^(-5)));
in_freq_dB_filt=in_freq_dB-vIsoThrDB';
in_freq_dB_filt=10.^(in_freq_dB_filt/20)*10^(-5);
out_freq=in_freq_dB_filt.*exp(1i*in_freq_phase);
out_freq=out_freq.';

%% output
out=real(ifft(out_freq)*len_in);

