function [results] = mrGPSM_feature_out(signal,ref,work,work_ref,simwork,dc2mod,def)
% mrGPSM_feature_out.m This function calculates based on envelope power and power SNRs 
%                      the final combined SNR from which speech reception
%                      thresholds are estimated.
% 
% 
% INPUT:
%                     signal    ...   signal + noise
%                        ref    ...   noise
%                       work    ...   some side-calculations on
%                                     signal+noise
%                   work_ref    ...   some side-calculations on noise
%                    simwork    ...   struct for parameters applied during
%                                     simulation, e.g., filter coefficients
%                     dc2mod    ...   3-dim output matrix containing
%                                     power-based values to weight envelope
%                                     power SNRs
%                        def    ...   struct for various parameters, e.g.,fs
% 
% OUTPUT:
%             results.SNRenv    ...   SNR-value from combining
%                                     envelope-power- and power-based SNRs
%          results.SNRenv_ac    ...   Envelope-power-based SNRs          
%          results.SNRenv_dc    ...   Power-based SNRs
%          results.SNRenv_dB    ...   Combined SNR-value in dB
%        results.fcenter_mod    ...   Center frequencies of the modulation
%                                     filters
%        results.fcenter_aud    ...   Center frequencies of the auditory
%                                     filters
%          results.exPattern    ...   Excitation patterns of modulation
%                                     channels per each auditory channel
%                                     for speech+noise and noise
%            results.SNR_mat    ...   2-d matrix containing SNRs, where
%                                     columns represents auditory channels
%                                     and rows represents the power and
%                                     modulation channels
%
% Usage: [results] = mrGPSM_feature_out(signal,ref,work,work_ref,simwork,dc2mod)
% author: thomas.biberger@uni-oldenburg
% date:   2016-10-09
% update: 2018-12-11, 2021-05-25


% some preallocations
[noWin noGTFB noMFB]=size(signal);
signal_temp=zeros(noGTFB,noMFB);
sig_exPattern=zeros(noGTFB,noMFB);
noise_exPattern=zeros(noGTFB,noMFB);
exPattern=zeros(2,noMFB,noGTFB);

validate_ref=work_ref.Longterm_DC';  % noise: long-term power per auditory channel
validate_sig=work.Longterm_DC';  % speech+noise: long-term power per auditory channel

%% set some restrictions/limitations in human sensitivity to amplitude modulation
ref=min(signal,ref); % ensure that the speech+noise power isn't lower than that of the noise signal
ref(:,:,2:end)=max(ref(:,:,2:end),0.002); % lower envelope power limit of -27 dB 
signal(:,:,2:end)=max(signal(:,:,2:end),0.002); % lower envelope power limit of -27 dB 


%% SNR calculation
SNR_temp_mod_per=(signal-ref)./ref; % SNR calculation from envelope power and power
dc2mod=max(dc2mod,0);
SNR_temp_mod_per=SNR_temp_mod_per.*dc2mod; % weighting of the envelope power SNRs according to the intensity in the corresponding auditory channel

%% temporal averaging across the temporal segments
for ii=1:noGTFB;
    for kk=1:length(work.inf_1);
        signal_temp(ii,kk)=nanmean(squeeze(SNR_temp_mod_per(1:work.nWin(ii,kk),ii,kk)),1);
        sig_exPattern(ii,kk)=nanmean(squeeze(signal(1:work.nWin(ii,kk),ii,kk)),1);
        noise_exPattern(ii,kk)=nanmean(squeeze(ref(1:work.nWin(ii,kk),ii,kk)),1);
    end
end

%% only enelope power SNRs above hearing threshold are considered (only relevant in near-threshold situations)
signal_temp=signal_temp';
signal_temp(2:end,validate_ref(1,:)<=1e-10 | validate_sig(1,:)<=1e-10)=0; %tb

%% only modulation filter center frequencies up to one fourth of the corresponding auditory channel center freq. are considered (Verhey et al.,1999)
if strcmp(def.predictions,'SI')==1;
ModFiltersMatrix = [[1:5 0 0 0 0]; [1:5 0 0 0 0]; [1:5 0 0 0 0]; [1:6 0 0 0]; [1:6 0 0 0]; [1:6 0 0 0];...
    [1:7 0 0] ; [1:7 0 0]; [1:7 0 0]; [1:8 0 ]; [1:8 0 ]; [1:8 0 ]; 1:9; 1:9; 1:9; 1:9;...
    1:9; 1:9; 1:9; 1:9; 1:9; 1:9;]';
ModFiltersMatrix=[ones(1,22);ModFiltersMatrix];
modFiltMat=ones(10,22);

elseif strcmp(def.predictions,'PSY')==1;
ModFiltersMatrix = [[1:5 0 0 0 0]; [1:5 0 0 0 0]; [1:5 0 0 0 0]; [1:6 0 0 0]; [1:6 0 0 0]; [1:6 0 0 0];...
    [1:7 0 0] ; [1:7 0 0]; [1:7 0 0]; [1:8 0 ]; [1:8 0 ]; [1:8 0 ]; 1:9; 1:9; 1:9; 1:9;...
    1:9; 1:9; 1:9; 1:9; 1:9; 1:9;1:9;1:9]';
ModFiltersMatrix=[ones(1,24);ModFiltersMatrix];
modFiltMat=ones(10,24);
else
     error('Invalid value of def.predictions!')
end
    
    
modFiltMat(ModFiltersMatrix==0)=0;
signal_temp_jeskLim=signal_temp.*modFiltMat;


%%  application of the band importance function to power features
if strcmp(def.predictions,'SI')==1;
    signal_temp_jeskLim(1,:)=signal_temp_jeskLim(1,:).*(simwork.BIF_adapted/max(simwork.BIF_adapted));
elseif strcmp(def.predictions,'PSY')==1;  
else
    error('Invalid value of def.predictions!')
end
signal_temp=signal_temp_jeskLim.^2;

%% combining SNRs across auditory/modulation channels
% here envelope power and power SNRs are regarded seperately as proposed in Biberger and
% Ewert (2016;2017)
%     % combining dc-channels
signal_temp_dc=signal_temp(1,:);
SNR_dc=sqrt(sum(signal_temp_dc,2));
%     % combining ac-channels
signal_temp_ac=signal_temp(2:end,:);
SNR_periph_ac=sum(signal_temp_ac,2);  % original tb
SNR_ac=sqrt(sum(SNR_periph_ac,1));    % original tb

%% combining & weighting AC- & DC-SNRs
SNR=max(SNR_ac,SNR_dc);

%%  output
exPattern(1,:,:)=sig_exPattern(:,1:end)';  % without DC-channel
exPattern(2,:,:)=noise_exPattern(:,1:end)'; % witout DC-channel

results.SNR_comb=SNR;
results.SNR_ac=SNR_ac;
results.SNR_dc=SNR_dc;
results.SNR_comb_dB=10*log10(SNR);
results.fcenter_mod=work.inf_1;
results.fcenter_aud=simwork.analyzer.center_frequencies_hz;
results.exPattern=exPattern;
results.SNR_mat=signal_temp_jeskLim;
end