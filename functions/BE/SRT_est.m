function [out]=SRT_est(SNRs,Pc,SRT)
% SRT_est.m This function estimates typically the speech reception
%           threshold (SRT) at which 50% of speech was understood.(SRT=50)
%           However, other SRTs like SRT80 can also be estimated by replacing
%           SRT=50 through SRT=80.
% INPUT:
%       SNRs ... Vector with SNRs between target speech and masker 
%       Pc   ... Vector with model-based predictions for percent correct
%                answers for each SNR-value
%       SRT  ... desired speech reception threshold (by default: 50%)
% 
% OUTPUT:
%       out  ... estimated SNR at which 50% (by default) percent of speech was understood 
% Usage: [out]=SRT_est(SNRs,Pc,SRT)
% author: thomas.biberger@uni-oldenburg.de
% date: 2015-04-15
% update: 2019-02-19

SNRs=SNRs(:);
Pc=Pc(:);

if Pc(end)==100
% check when Pc= 100%
[idx]=find(Pc==100);
out=(interp1(Pc(1:idx),SNRs(1:idx),SRT)); 
else 
    out=(interp1(Pc,SNRs,SRT)); 
end



