function [def simdef simwork]= mrGPSM_init(def,simdef)
% mrGPSM_init.m     some pre-calculations of e.g., filter coefficients that have
%                   been calculated only one time
%
% INPUT:
%       def:        struct for various parameters, e.g.,fs
%    simdef:        struct for model related parameters, e.g., filter order,
%                   cut-off frequency
%
% OUTPUT:
%       def:        struct for stimulus related parameters,e.g.,fs
%       simdef:     struct for model related parameters, e.g., filter order,
%                   cut-off frequency
%       simwork:    struct for parameters applied during simulation, e.g.,
%                   filter coefficients
%
% Usage: [def simdef simwork]= mrGPSM_init(def,simdef)
% thomas.biberger@uni-oldenburg.de;
% date: 2018-11-27
% update: 2021-05-19


%% Resampling
if strcmp(simdef.resampling,'on')==1;
    def.downsample_factor=2;                 % downsample signal by factor 2
    def.samplerate_down=def.samplerate/def.downsample_factor;  
elseif strcmp(simdef.resampling,'off')==1;
    def.samplerate_down=def.samplerate;     % no downsampling
else
    disp('Please define if resampling should be used!')
end

%% parameter (CFs etc.) of the GTFB 
simwork.analyzer = gfb_analyzer_new_mrGPSM(def.samplerate, simdef.gt_MinCF, simdef.gt_align, simdef.gt_MaxCF,...
                            simdef.gt_Dens,def);

%% iso hearing threshold
vsDesF=0:def.samplerate/def.intervallen:floor((def.intervallen/2))*def.samplerate/def.intervallen;   % NEW: freq vector needed for isothr
[simwork.vIsoThrDB, simwork.vsF] = iso389_7(vsDesF);

%% 1st order butterworth-filter for 150Hz-envelope-lp
simdef.env_lp_order=1;
simdef.env_lp_cutoff=150;
simdef.env_lp_cutoff_norm=simdef.env_lp_cutoff/(def.samplerate/2);
[simwork.b_env_lp,simwork.a_env_lp] = butter(simdef.env_lp_order,simdef.env_lp_cutoff_norm,'low');


%% Calculate audio channel dependent duration of time frames

% Speech intelligiblity: auditory filter center frequencies from 63 Hz to 8 kHz 
if strcmp(def.predictions,'SI')==1;
    cf=[63    80   100  125  160  200    250    315  400   500  630 800  1000  1250,...
        1600  2000 2500 3150 4000 5000 6300 8000];
% Psychoacoustics: auditory filter center frequencies from 63 Hz to 12.5 kHz
elseif strcmp (def.predictions,'PSY')==1;
    cf=[63    80   100  125  160  200    250    315  400   500  630 800  1000  1250,...
        1600  2000 2500 3150 4000 5000 6300 8000 10000 12500];
else
    error('Invalid value of def.predictions!')
end

% Calculate frequency dependent window length
octaafCF=[100,200,400,800,1000,2000,3000,4000,5000,7000,8000];
Gap=[18,9.5,7,6.5,6,6,4.5,4,4,3.9,3.7]; % Moore et al. 1993 
cf=min(max(cf,100),8000);
windowCB=floor(interp1(octaafCF,Gap,cf)); % default window length for each


% duration of window length depend if forward masking is switched on, otherwise
% longer windows are applied as it was suggested in Rhebergen et al.
% (2006)
if strcmp(simdef.fmf,'on')==1;
    simwork.STint_len=windowCB;
elseif strcmp(simdef.fmf,'off')==1;
    simwork.STint_len=ceil(windowCB.*2.5);
end

% Either using frequency dependent window length or a fixed window length
if strcmp(simdef.fix_win_len,'on')==1;
    simwork.STint_len(:)=12; % fixed window length of 12ms
elseif strcmp(simdef.fix_win_len,'off')==1;
end

%% Calculate band importance weighting function (BIF)
onethirdOct_CF=[160,200,250,315,400,500,630,800,1000,1250,1600,2000,2500,...
    3150,4000,5000,6300,8000];

% Band importance function -> Table 3 from ANSI S3.5-1997
BIF=[0.0083, 0.0095, 0.0150, 0.0289, 0.0440, 0.0578, 0.0653, 0.0711,...
    0.0818,0.0844, 0.0882, 0.0898, 0.0868, 0.0844, 0.0771,...
    0.0527, 0.0364, 0.0185]; %band importance function ANSI S3.5-1997

% Band importance function -> SPIN from ANSI S3.5-1997
% BIF=[0.0000, 0.0255, 0.0256, 0.0360, 0.0362, 0.0514, 0.0616, 0.0770,...
%     0.0718,0.0718, 0.1075, 0.0921, 0.1026, 0.0922, 0.0719,...
%     0.0461, 0.0306, 0.000]; %band importance function ANSI S3.5-1997


cf=min(max(cf,100),8000);
BIF_adapted=(interp1(onethirdOct_CF,BIF,cf)); 
BIF_adapted(cf<min(onethirdOct_CF))=BIF(1);
simwork.BIF_adapted=BIF_adapted;

end
